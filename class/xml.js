const fs = require('fs');

const { exec } = require("child_process");

module.exports = {

  generate_xml: (data, callback) => {

    xml_file(data, function(xml){
      var filename = "generated_data"
      var path = "tmp/" + filename +".xml";
      var options = "";

      fs.writeFile( path, xml, options, function(){
        generate_density(filename, function(){

          //remove file xml
          fs.unlink(path, (err) => {
            if (err) {
              console.error(err)

            }
          callback()
          })

        })

      })
    })

  },


  download_file: (data, callback) => {
    xml_file(data, function(xml){
      callback(xml);
    })
  }

}


function xml_file(data, callback){
  var xml = `
  <?xml version="${data.xml_version}" ?>

  <!--
  Parameters set by the user.
  All user input variables and all data in external input files must be in the same system of units.
  -->

  <!-- Coordinates of the full domain where the grid density function is greater than or equal to zero.
  The coordinates are given as: x0 x1 y0 y1 z0 z1. -->
  <fullDomain>${data.fullDomain}</fullDomain>

  <!-- All the levels are given, along with their respective box domains.
  The level-id in this file is disregarded. What matters is the order
  the levels are given. The levels must start with level 1 and increase.
  Level 0 is NOT provided, it is deduced by the algorithm. -->

  ${data.domains.map((level, level_id) =>
    `<level id="${level_id + 1}">

    ${level.map((domain, domain_id) =>
      `<domain id="${domain_id}">${domain}</domain>`
    ).join("\r\n")}

    </level>
    `).join("\r\n")}


    <!-- Number of points in the x-coordinate direction, for the discretization of the grid density function. -->
    <N>${data.resolution}</N>

    <!-- Number of times to smooth the grid density function before output. -->
    <numSmooth>${data.smooth}</numSmooth>
    `;

    callback(xml);


  }

  function generate_density(filename, callback){

    var file_path = "tmp/" + filename + ".xml";


    exec("./palabos/generateGridDensityFromBoxes/generateGridDensityFromBoxes " + file_path, (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        callback('error')
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        callback('error')
      }
      console.log(`stdout: ${stdout}`);
      callback();
    });
    /*
    exec("ls -la", (error, stdout, stderr) => {
    if (error) {
    console.log(`error: ${error.message}`);
    return;
  }
  if (stderr) {
  console.log(`stderr: ${stderr}`);
  return;
}
console.log(`stdout: ${stdout}`);
});
*/
}
