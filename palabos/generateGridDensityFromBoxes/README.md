# Grid density generation

The aim of this program is the generation of a grid density field with values between
0 and 1 that are then used by the mesh generation in grid refinement simulations.

The higher the grid denisity the finer the grid will be, the lower the grid density
the coarser the grid.

In this example we are going to construct a grid density for the flow past a sphere
using parallelepipedic domains.

## Compilation

To compile this project

```bash
mkdir build

cd build
cmake .. && make
cd ..
```

## Execution

```bash
./generateGridDensityFromBoxes simpleSphere_exercise.xml
```

## The sphere

The geometry of the sphere can be read from `sphere.stl`. It represents a diameter one
sphere in [STL](https://bit.ly/2YeMxtF) file format which center is at position `(0, 0, 0)`
and has a diameter of one.
One can read it with the [Paraview](https://www.paraview.org/) library.

## Parameters

The parameters for the grid density generation are read from the `simpleSphere_exercise.xml` file. There are four different
kind of parameters:

1. The `fullDomain` which is the bounding box in physical units of the simulation domain.
2. The different levels which contain rectangular domains. Each level represent a different grid density
value and can contain many rectangular domains. The grid density of each domain is automatically
adjusted between 0 and 1.
3. The resolution `N` which is the number of grid points in the `x` direction.
4. And the number of smoothing operations `numSmooth` which is larger or equal to zero
and helps make transitions between grid density values smoother. The smoothing operation is nothing else than
a Laplacian filter.

## Output

The are two output files. One `gridDensity.vti` file that can be visualized with the Paraview
softwarre and a `gridDensity.dat` file that is an ASCII file containing the grid density
at each mesh point, as well as the resolutoin and full domain size.

# Exercises

## Add boxes and smooth

1. Add a finer level of size $\vec x_0 = (-1.5, -1.5, -1.5)$ and $\vec x_1=(4.5, 1.5, 1.5)$.
2. Add another domain in level 2.
3. Try smoothing the domain a bit further.
4. Vizualize the newly created domains with the sphere using Paraview.
5. What happens if one adds two levels with the same id, containing different domains?
6. What happens if one adds different domains in a single level?

## Remark

For this exercise you will need to modify places in the `simpleSphere_exercise.xml` file.
The places that require modifications can be found by looking for the keyword `Exercise`.
